from argparse import ArgumentParser, RawTextHelpFormatter

from ip2geotools.databases.noncommercial import DbIpCity, InvalidRequestError
from typing import Tuple

import ipinfo
import pathlib

from geopy.geocoders import Nominatim

from geolite2 import geolite2

import json
import random

class LocationScrambler:
	"""
	Scramble locations if they are the same
	"""
	def __init__(self):
		self.locations = set()

	def get_location(self, latitude, longitude):
		latitude = float(latitude)
		longitude = float(longitude)
		if (latitude, longitude) in self.locations:
			latitude +=  random.random()/(5*10)
			longitude +=  random.random()/(5*10)

		self.locations.add((latitude, longitude))
		return (latitude, longitude)

class Geolocation:
	def __init__(self, latitude, longitude, cache = True, provider="unkown"):
		self.longitude = longitude
		self.latitude = latitude
		self.cache = cache
		self.provider = provider



class GeoProvider_geopy:
	def __init__(self):
		self.geolocator = Nominatim(user_agent="geolocation")

	def get_location(self, location):
		location = self.geolocator.geocode(location)
		return Geolocation(location.latitude, location.longitude, provider=self.__class__.__name__)


class GeoIPProvider_geolite2:
	def __init__(self):
		self.reader = geolite2.reader()

	def get_location(self, ip):
		match = self.reader.get(ip)
		if match and "location" in match:
			latitude = match["location"]["latitude"]
			longitude = match["location"]["longitude"]
			return Geolocation(latitude, longitude, provider=self.__class__.__name__)
		return None


class GeoIPProvider_ipinfo:
	def __init__(self):
		self.handler = ipinfo.getHandler()
		self.limit_reached = False

	def get_location(self, ip:str):
		if not self.limit_reached:
			try:
				details = self.handler.getDetails(ip)
				return Geolocation(details.latitude, details.longitude, provider=self.__class__.__name__)
			except ipinfo.exceptions.RequestQuotaExceededError:
				print("[GeoIPProvider_ipinfo] Limit reached. Disabling")
				self.limit_reached = True
		return None



class GeoIPProvider_ip2geotools:
	def __init__(self):
		self.invalid_requests = 0

	def get_location(self, ip:str) -> Geolocation:
		if self.invalid_requests < 5:
			try:
				coords = DbIpCity.get(ip, api_key='free')
				if coords.latitude is not None and coords.longitude is not None:
					self.invalid_requests = 0
					return Geolocation(coords.latitude, coords.longitude, provider=self.__class__.__name__)
			except InvalidRequestError:
				self.invalid_requests += 1
				print("[ip2geotools] InvalidRequestError. Maybe you are blocked from the provider?")
		return None


class GeoIPProvider:
	def __init__(self):
		self.data = {}
		self.provider_list = [GeoIPProvider_geolite2()]
		self.provider_remote_list = [GeoIPProvider_ip2geotools(), GeoIPProvider_ipinfo()]
		self.provider_country = []

	def _get_location_provider(self, provider_list, data):
		for provider in provider_list:
			coords = provider.get_location(data)
			if coords is not None:
				print("Found data in provider {}".format(provider.__class__.__name__))
				return coords
		return None

	def get_location(self, ip:str, country: str = None) -> Tuple[Tuple[float, float], str]:
		provider_list_list = [(self.provider_remote_list, True, ip), (self.provider_list, False, ip)]
		for provider_list in provider_list_list:
			coords = self._get_location_provider(provider_list[0], provider_list[2])
			if coords is not None:
				return coords
		return None

