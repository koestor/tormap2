#!/bin/bash

declare -A NPM_DEPENDENCIES
NPM_DEPENDENCIES=(
	["leaflet"]="leaflet.js leaflet.css"
	["leaflet.markercluster"]="leaflet.markercluster.js MarkerCluster.css MarkerCluster.Default.css"
	["leaflet.awesome-markers"]="leaflet.awesome-markers.min.js leaflet.awesome-markers.css images"
	["sql.js"]="sql-wasm.js sql-wasm.wasm worker.sql-wasm.js"
)

pushd html || exit 1
mkdir -p dist

for dep in "${!NPM_DEPENDENCIES[@]}"; do
	npm install "${dep}"
	files="${NPM_DEPENDENCIES[${dep}]}"
	files_arr=(${files})
	for file in "${files_arr[@]}"; do
		echo "$file"
		cp -r "node_modules/${dep}/dist/${file}" "./dist/"
	done
done
rm -rf node_modules
popd || exit 1
