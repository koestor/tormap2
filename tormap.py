#!/usr/bin/env python3

import urllib3
import pathlib

from sqlalchemy import Column, String, Integer, Text, orm, Float, ForeignKey, create_engine, Boolean
from sqlalchemy.ext.declarative import declarative_base

import json

from dateutil import parser
import time

from collections import defaultdict

from geoip_provider import GeoIPProvider

import subprocess

SQLBase = declarative_base()

class RelayFlag(SQLBase):
	__tablename__ = "RelayFlag"
	query: orm.Query = None


	fingerprint: str = Column(String(255), ForeignKey("RelayEntry.fingerprint"), primary_key=True)
	flag: str = Column(String(255), primary_key=True)

	def __init__(self, fingerprint, flag):
		self.fingerprint = fingerprint
		self.flag = flag

class RelayIP(SQLBase):
	__tablename__ = "RelayIP"
	query: orm.Query = None

	fingerprint: str = Column(String(255), ForeignKey("RelayEntry.fingerprint"), primary_key=True)
	ip: str = Column(String(255), primary_key=True)
	port: str = Column(String(255), primary_key=True)

	def __init__(self, fingerprint, ip_port):
		self.fingerprint = fingerprint
		ip, _, port = ip_port.rpartition(':')
		self.ip = ip.strip("[]")
		self.port = port

class RelayEntry(SQLBase):
	__tablename__ = "RelayEntry"
	query: orm.Query = None

	nickname: str = Column(String(255))
	fingerprint: str = Column(String(255), primary_key=True)
	# or_addresses we assume only one ipv4 and one ipv6 per relay. seems reasonable
	ipv4: str = Column(String(255))
	ipv4_port: int = Column(Integer)
	ipv6: str = Column(String(255))
	ipv6_port: int = Column(Integer)
	last_seen: int = Column(Integer)
	last_changed_address_or_port: int = Column(Integer)
	first_seen: int = Column(Integer)
	running: bool = Column(Boolean)
	# flags
	country: str = Column(String(255))
	country_name: str = Column(String(255))
	as_id: str = Column(String(255))
	# TODO: own table for this mapping? Same for country
	as_name: str = Column(String(255))
	consensus_weight: int = Column(Integer)
	# verified_host_names
	last_restarted: int = Column(Integer)
	bandwidth_rate = Column(Integer)
	bandwidth_burst = Column(Integer)
	observed_bandwidth = Column(Integer)
	advertised_bandwidth: int = Column(Integer)
	# exit_policy
	# exit_policy_summary
	platform: str = Column(String(255))
	version: str = Column(String(255))
	version_status: str = Column(String(255))
	# effective_family
	consensus_weight_fraction: float = Column(Float)
	guard_probability: float = Column(Float)
	middle_probability: float = Column(Float)
	exit_probability: float = Column(Float)
	recommended_version: bool = Column(Boolean)
	measured: bool = Column(Boolean)

	def __get_unix_time(self, date_str):
		date = parser.parse(date_str)
		return date.timestamp()


	def __get_float(self, d, key, default = 0.0):
		try:
			return float(d[key])
		except:
			return default
	
	def __add_ip(self, ip_port):
		ip, _, port = ip_port.rpartition(':')
		if "[" in ip:
			self.ipv6 = ip.strip("[]")
			self.ipv6_port = port
		else:
			self.ipv4 = ip
			self.ipv4_port = port

	def __init__(self, db, relay_dict):
		self.db = db
		relay_dict = defaultdict(str, relay_dict)
		try:
			self.nickname = relay_dict["nickname"]
			self.fingerprint = relay_dict["fingerprint"]
			# or_addresses
			for addr in relay_dict["or_addresses"]:
				self.__add_ip(addr)
			self.last_seen = self.__get_unix_time(relay_dict["last_seen"])
			self.last_changed_address_or_port = self.__get_unix_time(relay_dict["last_changed_address_or_port"])
			self.first_seen = self.__get_unix_time(relay_dict["first_seen"])
			self.running = relay_dict["running"] == "true"
			# flags
			for flag in relay_dict["flags"]:
				flag_obj = RelayFlag(self.fingerprint, flag)
				self.db.add(flag_obj)
			self.country = relay_dict["country"]
			self.country_name = relay_dict["country_name"]
			self.as_id = relay_dict["as"]
			self.as_name = relay_dict["as_name"]
			self.consensus_weight = int(relay_dict["consensus_weight"])
			# verified_host_names
			self.last_restarted = self.__get_unix_time(relay_dict["last_restarted"])

			self.bandwidth_rate = int(relay_dict["bandwidth_rate"])
			self.bandwidth_burst = int(relay_dict["bandwidth_burst"])
			self.observed_bandwidth = int(relay_dict["observed_bandwidth"])
			self.advertised_bandwidth = int(relay_dict["advertised_bandwidth"])
			# exit_policy
			# exit_policy_summary
			self.platform = relay_dict["platform"]
			self.version = relay_dict["version"]
			self.version_status = relay_dict["version_status"]
			# effective_family
			self.consensus_weight_fraction = self.__get_float(relay_dict, "consensus_weight_fraction")
			self.guard_probability = self.__get_float(relay_dict, "guard_probability")
			self.middle_probability = self.__get_float(relay_dict, "middle_probability")
			self.exit_probability = self.__get_float(relay_dict, "exit_probability")
			self.recommended_version = relay_dict["recommended_version"] == "true"
			self.measured = relay_dict["measured"] == "true"
		except:
			print(relay_dict)
			raise

class IPLocation(SQLBase):
	__tablename__ = "IPLocation"
	query: orm.Query = None

	ip: str = Column(String(255), primary_key=True)
	source: str = Column(String(255), primary_key=True)
	latitude: float = Column(Float)
	longitude: float = Column(Float)
	time: int = Column(Integer)

	def __init__(self, ip: str, source: str, latitude: float, longitude: float, time: int) -> None:
		self.ip = ip
		self.source = source
		self.latitude = latitude
		self.longitude = longitude
		self.time = time
		

class OnionooData:

	def __init__(self, database_path):
		engine = create_engine(f"sqlite:///{database_path}")
		db_factory = orm.sessionmaker(bind=engine)
		db_session = orm.scoped_session(db_factory)
		SQLBase.metadata.bind = engine
		SQLBase.metadata.drop_all(tables=[RelayEntry.__table__, RelayFlag.__table__])
		SQLBase.metadata.create_all()
		self.db = db_session
		json_path = pathlib.Path("relays.json")
		if not json_path.exists():
			self.download_data(".")
		with open(json_path, "r") as json_file:
			json_data = json.loads(json_file.read())
			for relay in json_data["relays"]:
				relay_entry = RelayEntry(self.db, relay)
				self.db.add(relay_entry)
			self.db.commit()
		ips = set()
		for ip in self.db.query(RelayEntry.ipv4):
			ips.add(ip[0])
		geo_provider = GeoIPProvider()
		for ip in ips:
			table_ips = self.db.query(IPLocation).filter(IPLocation.ip == ip).all()
			# TODO: consider data source and time
			if len(table_ips) == 0:
				coords = geo_provider.get_location(ip, relay["country"])
				if coords is not None:
					ip_entry = IPLocation(ip, coords.provider, coords.latitude, coords.longitude, time.time())
					self.db.add(ip_entry)
		self.db.commit()


	def download_data(self, output_dir):
		print("Downloading json file")
		manager = urllib3.PoolManager()
		resp = manager.request("GET", "https://onionoo.torproject.org/details")
		if resp.status == 200:
			json_file = pathlib.Path(output_dir) / "relays.json"
			with open(json_file, "w") as o:
				o.write(resp.data.decode())

def dump_locations(db, cache):
	p = subprocess.Popen([f"sqlite3 {db} '.dump IPLocation' > {cache}"], shell=True)
	p.communicate()

def load_locations(db, cache):
	p = subprocess.Popen([f"sqlite3 {db} < {cache} 2>/dev/null"], shell=True)
	p.communicate()

if __name__ == "__main__":
	database_path = pathlib.Path("html/test.sqlite")
	load_locations(database_path, "geo_cache")
	d = OnionooData(database_path)
	dump_locations(database_path, "geo_cache")

	print("Done")
