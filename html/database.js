export class TorDatabase {
	constructor() {
		this.id = 1;
		this.worker = new Worker("dist/worker.sql-wasm.js");
		this.pending_queries = {}
		this.worker.onmessage = () => {
			console.log("Database opened");
			this.worker.onmessage = (e) => {
				let id = e.data.id;
				this.pending_queries[id][0](e.data.results);
				delete this.pending_queries[id];
    		};
		};

	}
	
	async init(database) {

		const sqlPromise = initSqlJs({
			locateFile: file => `dist/${file}`
		});
		const dataPromise = fetch(database).then(res => res.arrayBuffer());
		const [SQL, buf] = await Promise.all([sqlPromise, dataPromise])
		this.db = new SQL.Database(new Uint8Array(buf));

		this.worker.onerror = e => console.log("Worker error: ", e);
		this.worker.postMessage({
			id:this.id++,
			action:"open",
			buffer:buf, /*Optional. An ArrayBuffer representing an SQLite Database file*/
		});
	}

	async run_statement(statement) {
		/*
		let resp_array = []
		const stmt = this.db.prepare(statement);
		stmt.getAsObject({$start:1, $end:1});
		stmt.bind({$start:1, $end:2});
		while(stmt.step()) { //
			const row = stmt.getAsObject();
			resp_array.push(row)
		}
		stmt.free();
		*/

		let resolve, reject;
		let promise = new Promise((resolve, reject) => {
			this.pending_queries[this.id] = [resolve, reject];
		});
		this.worker.postMessage({
			id:this.id++,
			action: "exec",
			sql: statement
		});
		const result = await promise;
		let result_obj = []
		if (result && result[0]) {
			result[0].values.forEach((value_arr, index_arr) => {
				let obj = {}
				value_arr.forEach((v, i) => {
					let name = result[0].columns[i];
					obj[name] = v;
				});
				result_obj.push(obj);
			});
		}

		return result_obj;

	}

	get_relay(name) {
		const statement = "SELECT * FROM RelayEntry WHERE nickname LIKE '%" + name + "%';";
		let relay_array = this.run_statement(statement);
		return relay_array;
	}

	get_location(ip) {
		const statement = "SELECT * FROM IPLocation WHERE ip='" + ip + "';";
		return this.run_statement(statement);
	}

	get_ips(fingerprint) {
		const statement = "SELECT ip from RelayIP WHERE fingerprint='" + fingerprint + "';";
		return this.run_statement(statement);
	}
}
