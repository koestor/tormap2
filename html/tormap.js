import {TorDatabase} from "./database.js"

function timestamp_to_date_str(timestamp_s) {
	const timestamp_ms = timestamp_s * 1000;
	return new Date(timestamp_ms).toISOString();
}

function byte_to_formated(bytes) {
	let units = [ "B", "KiB", "MiB", "GiB", "TiB" ];
	for (let i = 0; i < units.length; ++i) {
		if (Math.abs(bytes) < 1024 || i == units.length - 1) {
			const output_str = "" + bytes.toFixed(2) + units[i];
			return output_str;
		}
		bytes /= 1024.0;
	}
}

class TorMap {
	constructor(db) {
		this.map = L.map('map').setView({lon: 0, lat: 0}, 2);
		
		// add the OpenStreetMap tiles
		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			maxZoom: 19,
			attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
		}).addTo(this.map);
		
		// show the scale bar on the lower left corner
		L.control.scale({imperial: false, metric: true}).addTo(this.map);
		
		this.db = db;
		this.layer_groups = {};
	}

	async set_layer(layer_id, relays, color) {
		console.log("Setting layer with color " + color);
		let marker_group = new L.MarkerClusterGroup({
			iconCreateFunction: function(cluster) {
				const childCount = cluster.getChildCount();
				let total_bandwidth = 0;
				const children = cluster.getAllChildMarkers();
				children.forEach((value) => {
					total_bandwidth += value.relay.observed_bandwidth;
				});
				let total_bandwidth_str = byte_to_formated(total_bandwidth) + "/s";
				return new L.DivIcon({
					// TODO: make visuals better
					html: `<div style="background-color:${color};"><span>${childCount}</span><span class="tooltip">Total Bandwidth: ${total_bandwidth_str}</span></div>`,
					className: 'marker-cluster',
					iconSize: new L.Point(40, 40)
				});

			}

		});
		relays.forEach(async (item, index) => {
			const loc = await this.db.get_location(item.ipv4);
			if(loc[0]) {
				item["latitude"] = loc[0]["latitude"]
				item["longitude"] = loc[0]["longitude"]
				let marker = this.get_marker(item, color);
				marker_group.addLayer(marker);
			}
		});

		if (this.layer_groups[layer_id] != undefined) {
			this.map.removeLayer(this.layer_groups[layer_id]);
			this.layer_groups[layer_id].remove();
		}
		this.layer_groups[layer_id] = marker_group;
		this.map.addLayer(marker_group);
	}


	get_marker(relay, color) {
		let template = `<Placemark>\n\
            <h1>${relay.nickname}</h1>\n\
            <description>\n\
            <p><strong>IPv4</strong>: <a href="https://centralops.net/co/DomainDossier.aspx?dom_whois=1&net_whois=1&dom_dns=1&addr=${relay.ipv4}">${relay.ipv4}:${relay.ipv4_port}</a></p>\n\
            <p><strong>IPv6</strong>: <a href="https://centralops.net/co/DomainDossier.aspx?dom_whois=1&net_whois=1&dom_dns=1&addr=${relay.ipv6}">${relay.ipv6}:${relay.ipv6_port}</a></p>\n\
            <p><strong>Directory Address</strong>: $dir_address</p>\n\
            <p><strong>Bandwidth</strong>: ${relay.observed_bandwidth * 1e-6} MB/s</p>\n\
            <p><strong>Flags</strong>: ${relay.flags}</p>\n\
            <p><strong>Up since</strong>: ${timestamp_to_date_str(relay.last_restarted)}</p>\n\
            <p><strong>Contact</strong>: $contact</p>\n\
            <p><strong>IPv4 Policy</strong>: $exit_policy_summary</p>\n\
            <p><strong>IPv6 Policy</strong>: $exit_policy_v6_summary</p>\n\
            <p><strong>Fingerprint</strong>: <a href="https://metrics.torproject.org/rs.html#details/${relay.fingerprint}">${relay.fingerprint}</a></p>\n\
            <p><strong>Country</strong>: ${relay.country_name}</p>\n\
            <p><strong>Platform</strong>: ${relay.platform}</p>\n\
            <p><strong>Recommended Version</strong>: ${relay.recommended_version}</p>\n\
            </description>\n\
            </Placemark>\n\
`
		var marker_icon = L.AwesomeMarkers.icon({
			markerColor: color
		});
		var marker = L.marker([relay.latitude, relay.longitude], {icon: marker_icon}).bindPopup(template);
		marker.relay = relay;
		return marker
	}
}

class TorFilter {
	constructor(name) {
		this.filter_dict = {};
		this.name = name;
		this.active = false;
		//FIXME: This should not handle the color!
		this.color = "blue";
	}

	set_active(active) {
		this.active = active == true;
	}
	
	set_color(color) {
		this.color = color;
		console.log("Set color to " + color);
	}

	set_filter(name, id, value, comparison = "=") {
		this.filter_dict[name] = [id, value, comparison];
	}

	remove_filter(name) {
		delete this.filter_dict[name];
	}

	get_sql_statement() {
		let base_statement = "SELECT *,GROUP_CONCAT(RelayFlag.flag) flags FROM RelayEntry INNER JOIN RelayFlag USING(fingerprint) GROUP BY fingerprint";
		if (Object.keys(this.filter_dict).length > 0) {
			base_statement += " HAVING";
			for(const [key, value] of Object.entries(this.filter_dict)) {
				base_statement += " " + value[0] + " " + value[2] + " '" + value[1] + "' AND";
			}
			base_statement = base_statement.substring(0, base_statement.length - 4);
		}
		//base_statement += " GROUP BY fingerprint;";
		base_statement += ";";
		console.log(base_statement);

		return base_statement;
	}

	async get_relays(db) {
		if (this.active) {
			const statement = this.get_sql_statement();
			return await db.run_statement(statement);
		}
		return [];
	}

	async update(db, map) {
		const relays = await this.get_relays(db)
		map.set_layer(this.name, relays, this.color);
	}
}

class FilterConfig {
	constructor(filter, db, map) {
		this.db = db;
		this.map = map;
		this.init(filter);
	}

	async init(filter) {
		const filters_div = document.getElementById("filter_settings");
		let current_filter_div = document.createElement("div");
		current_filter_div.id = "filter_settings_" + filter["name"];
		current_filter_div.style.borderColor = filter["color"];

		let current_filter_header = document.createElement("div");
		current_filter_header.className = "filter_header";
		// Add collapsable button
		const collapsible_button = document.createElement("button");
		collapsible_button.className = "collapsible";
		collapsible_button.innerText = filter["name"];
		collapsible_button.addEventListener("click", (e) => {
			e.target.parentElement.classList.toggle("filter_header_active");
			let content = e.target.parentElement.nextElementSibling;
			if (content.style.maxHeight) {
				content.style.maxHeight = null;
			}
			else {
				content.style.maxHeight = content.scrollHeight + "px";
			}
		});

		let activate_checkbox = document.createElement("input");
		activate_checkbox.type = "checkbox";
		activate_checkbox.addEventListener("change",(e) => {
			 // TODO: change visuals
			tor_filter.set_active(e.target.checked);
			tor_filter.update(this.db, this.map);
		});

		let current_filter_content = document.createElement("div");
		current_filter_content.className = "collapsible_content";

		let tor_filter = new TorFilter(filter["name"]);
		tor_filter.set_color(filter["color"]);
		tor_filter.set_active(false);

		// Add children
		filters_div.appendChild(current_filter_div);
		current_filter_header.appendChild(activate_checkbox);
		current_filter_header.appendChild(collapsible_button);
		current_filter_div.appendChild(current_filter_header);
		current_filter_div.appendChild(current_filter_content);

		filter["filter_fields"].forEach((filter_field) => {
			if ("label" in filter_field) {
				const label = document.createElement("label");
				label.innerText = filter_field["label"];
				current_filter_content.appendChild(label);
			}
			const elem = document.createElement("input");
			if ("value" in filter_field) {
				elem.value = filter_field["value"];
			}
			elem.type = filter_field["type"];
			current_filter_content.appendChild(elem);
			let elem_func = async (target) => {
				console.log("Elem func!!");
				if (target.type == "checkbox" && !target.checked || target.value == "") {
					tor_filter.remove_filter(filter_field["label"]);
				} else {
					let search_val = target.value;
					let search_comp = "=";
					if (filter_field["search_type"] == "partial") {
						search_comp = "LIKE";
						search_val = "%" + search_val + "%";
					}
					else if (filter_field["search_type"] == "greater") {
						search_comp = ">";
					}
					else if (filter_field["search_type"] == "less") {
						search_comp = "<";
					}
					if(filter_field["multiplier"]) {
						search_val *= filter_field["multiplier"];
					}
					tor_filter.set_filter(filter_field["label"], filter_field["id"], search_val, search_comp);
				}
				tor_filter.update(this.db, this.map);
			};
			if (filter_field["active"] == true) {
				elem.click();
				elem_func(elem);
			}
			let timeout = null;
			elem.addEventListener(filter_field["trigger"], (e) => {
				clearTimeout(timeout);
				timeout = setTimeout(() => {
				elem_func(e.target)
				}, filter_field["delay"]);
			});
		});
		if (filter["active"] == true) {
			activate_checkbox.checked = true;
			tor_filter.set_active(true);
			tor_filter.update(this.db, this.map);
		}
		console.log(filter["active"]);
	}

}

async function load_config(db, map, path = "filter_config.json") {
	const dataPromise = fetch(path).then(res => res.json());
	let filter_config = await dataPromise
	let configs = []
	filter_config["filters"].forEach((filter) => {
		configs.push(new FilterConfig(filter, db, map));
	});
}


async function init_map() {
	let db = new TorDatabase();
	await db.init("test.sqlite");

	let map = new TorMap(db);

	await load_config(db, map);
}

init_map();
